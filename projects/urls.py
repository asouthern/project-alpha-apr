from django.urls import path
from projects.views import see_projects, detail_view, create_project


urlpatterns = [
    path("", see_projects, name="list_projects"),
    path("<int:id>/", detail_view, name="show_project"),
    path("create/", create_project, name="create_project"),
]
