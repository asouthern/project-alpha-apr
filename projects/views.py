from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import CreateProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def see_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def detail_view(request, id):
    detail = Project.objects.get(id=id)
    context = {
        "detail": detail,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            proj = form.save(False)
            proj.owner = request.user
            proj.save()
            return redirect("list_projects")

    else:
        form = CreateProjectForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
